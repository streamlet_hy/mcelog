Summary:	Tool to decode kernel machine check log on x86 machines
Name:		mcelog
Version:	194
Release:	3%{?dist}
License:	GPLv2
URL:		https://github.com/andikleen/mcelog
Source0:	https://github.com/andikleen/mcelog/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:	mcelog.conf
ExclusiveArch:	x86_64 loongarch64
BuildRequires:	make gcc systemd
Requires(post):	systemd
Requires(preun):systemd
Requires(postun):systemd

%description
mcelog logs and accounts machine checks (in particular memory, IO, and CPU hardware errors) on modern x86 Linux systems.

%prep
%autosetup

%build
# automatically populate the .os_version file so that "mcelog --version"
# returns a valid value instead of "unknown"
echo "%{version}-%{release}" > .os_version
%make_build CFLAGS="$RPM_OPT_FLAGS -fpie -pie"

%install
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog
install -p -m644 %{SOURCE1} $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/mcelog.conf
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/triggers
install -p -m755 triggers/cache-error-trigger $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/triggers/cache-error-trigger
install -p -m755 triggers/dimm-error-trigger $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/triggers/dimm-error-trigger
install -p -m755 triggers/page-error-trigger $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/triggers/page-error-trigger
install -p -m755 triggers/socket-memory-error-trigger $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/triggers/socket-memory-error-trigger
mkdir -p $RPM_BUILD_ROOT/%{_unitdir}
install -p -m644 mcelog.service $RPM_BUILD_ROOT%{_unitdir}/mcelog.service
mkdir -p $RPM_BUILD_ROOT/%{_sbindir}
install -p -m755 mcelog $RPM_BUILD_ROOT/%{_sbindir}/mcelog
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man{5,8}
install -p -m644 mcelog*.8 $RPM_BUILD_ROOT/%{_mandir}/man8/
install -p -m644 mcelog*.5 $RPM_BUILD_ROOT/%{_mandir}/man5/

%post
%systemd_post mcelog.service

%preun
%systemd_preun mcelog.service

%postun
%systemd_postun_with_restart mcelog.service

%files
%{_sbindir}/mcelog
%dir %{_sysconfdir}/mcelog
%{_sysconfdir}/mcelog/triggers
%config(noreplace) %{_sysconfdir}/mcelog/mcelog.conf
%{_unitdir}/mcelog.service
%{_mandir}/*/*

%changelog
* Thu Mar 19 2024 Huang Yang <huangyang@loongson.cn> - 194-3
- add loongarch64 support

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 194-2
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Jul 14 2023 Xiaojie Chen <jackxjchen@tencent.com> - 194-1
- Upgrade to upstream version 194

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 182-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 182-2
- Rebuilt for OpenCloudOS Stream 23

* Fri Jun 24 2022 Songqiao Tao <joeytao@tencent.com> - 182-1
- initial build
